Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post 'webhooks/sign_up', to: 'webhooks#sign_up'
  post 'webhooks/sign_in', to: 'webhooks#sign_in'
  post 'webhooks/new_session', to: 'webhooks#new_session'
  post 'webhooks/real_estate_listing_search', to: 'webhooks#real_estate_listing_search'
  post 'webhooks/real_estate_listing_favorite', to: 'webhooks#real_estate_listing_favorite'
  post 'webhooks/real_estate_listing_unfavorite', to: 'webhooks#real_estate_listing_unfavorite'
  post 'webhooks/real_estate_listing_info_request', to: 'webhooks#real_estate_listing_info_request'
  post 'webhooks/app_view_visit', to: 'webhooks#app_view_visit'
  post 'webhooks/selected_agent', to: 'webhooks#selected_agent'
end
