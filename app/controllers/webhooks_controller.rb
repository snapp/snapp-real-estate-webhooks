class WebhooksController < ApplicationController
  def new_session
    app_session = AppSession.new(
      event_type: params[:event_type],
      reference_id: params[:data][:reference_id],
      app_id: params[:data][:app_id],
      device_id: params[:data][:device_id],
      user_id: params[:data][:user_id],
      device_type: params[:data][:device_type],
      device_os: params[:data][:device_os],
      timezone: params[:data][:timezone],
      locale: params[:data][:locale],
      language: params[:data][:language],
      occurred_at: params[:data][:occurred_at]
    )

    if app_session.save
      render status: 200, json: {}
    else
      render status: 400, json: {}
    end
  end

  def sign_up
    user_sign_up = UserSignUp.new(
      event_type: params[:event_type],
      reference_id: params[:data][:reference_id],
      user_id: params[:data][:user_id],
      app_id: params[:data][:app_id],
      device_id: params[:data][:device_id],
      first_name: params[:data][:first_name],
      last_name: params[:data][:last_name],
      email: params[:data][:email],
      occurred_at: params[:data][:occurred_at],
      selected_agent_email: params[:data][:selected_agent_email]
    )

    if user_sign_up.save
      render status: 200, json: {}
    else
      render status: 400, json: {}
    end
  end

  def sign_in
    user_sign_in = UserSignIn.new(
      event_type: params[:event_type],
      reference_id: params[:data][:reference_id],
      user_id: params[:data][:user_id],
      app_id: params[:data][:app_id],
      device_id: params[:data][:device_id],
      first_name: params[:data][:first_name],
      last_name: params[:data][:last_name],
      email: params[:data][:email],
      occurred_at: params[:data][:occurred_at],
      selected_agent_email: params[:data][:selected_agent_email]
    )

    if user_sign_in.save
      render status: 200, json: {}
    else
      render status: 400, json: {}
    end
  end

  def real_estate_listing_search
    search = RealEstateListingSearch.new(
      event_type: params[:event_type],
      reference_id: params[:data][:reference_id],
      user_id: params[:data][:user_id],
      app_id: params[:data][:app_id],
      device_id: params[:data][:device_id],
      search_type: params[:data][:search_type],
      search_params: params[:data][:search_params],
      occurred_at: params[:data][:occurred_at]
    )

    if search.save
      render status: 200, json: {}
    else
      render status: 400, json: {}
    end
  end

  def real_estate_listing_favorite
    favorite = RealEstateListingFavorite.new(
      event_type: params[:event_type],
      reference_id: params[:data][:reference_id],
      user_id: params[:data][:user_id],
      app_id: params[:data][:app_id],
      device_id: params[:data][:device_id],
      occurred_at: params[:data][:occurred_at],
      real_estate_listing_id: params[:data][:real_estate_listing][:id],
      real_estate_listing_url: params[:data][:real_estate_listing][:url],
      real_estate_listing_mls_number: params[:data][:real_estate_listing][:mls_number],
      real_estate_listing_mls: params[:data][:real_estate_listing][:mls],
      real_estate_listing_address: params[:data][:real_estate_listing][:address],
      real_estate_listing_city: params[:data][:real_estate_listing][:city],
      real_estate_listing_state: params[:data][:real_estate_listing][:state],
      real_estate_listing_zipcode: params[:data][:real_estate_listing][:zipcode]
    )

    if favorite.save
      render status: 200, json: {}
    else
      render status: 400, json: {}
    end
  end

  def real_estate_listing_unfavorite
    unfavorite = RealEstateListingUnfavorite.new(
      event_type: params[:event_type],
      reference_id: params[:data][:reference_id],
      user_id: params[:data][:user_id],
      app_id: params[:data][:app_id],
      device_id: params[:data][:device_id],
      occurred_at: params[:data][:occurred_at],
      real_estate_listing_id: params[:data][:real_estate_listing][:id],
      real_estate_listing_url: params[:data][:real_estate_listing][:url],
      real_estate_listing_mls_number: params[:data][:real_estate_listing][:mls_number],
      real_estate_listing_mls: params[:data][:real_estate_listing][:mls],
      real_estate_listing_address: params[:data][:real_estate_listing][:address],
      real_estate_listing_city: params[:data][:real_estate_listing][:city],
      real_estate_listing_state: params[:data][:real_estate_listing][:state],
      real_estate_listing_zipcode: params[:data][:real_estate_listing][:zipcode]
    )

    if unfavorite.save
      render status: 200, json: {}
    else
      render status: 400, json: {}
    end
  end

  def real_estate_listing_info_request
    info_request = RealEstateListingInfoRequest.new(
      event_type: params[:event_type],
      reference_id: params[:data][:reference_id],
      user_id: params[:data][:user_id],
      app_id: params[:data][:app_id],
      device_id: params[:data][:device_id],
      agent_email: params[:data][:agent_email],
      user_email: params[:data][:user_email],
      user_name: params[:data][:user_name],
      user_phone: params[:data][:user_phone],
      user_question: params[:data][:user_question],
      requested_showing_date: params[:data][:requested_showing_date],
      occurred_at: params[:data][:occurred_at],
      real_estate_listing_id: params[:data][:real_estate_listing][:id],
      real_estate_listing_url: params[:data][:real_estate_listing][:url],
      real_estate_listing_mls_number: params[:data][:real_estate_listing][:mls_number],
      real_estate_listing_mls: params[:data][:real_estate_listing][:mls],
      real_estate_listing_address: params[:data][:real_estate_listing][:address],
      real_estate_listing_city: params[:data][:real_estate_listing][:city],
      real_estate_listing_state: params[:data][:real_estate_listing][:state],
      real_estate_listing_zipcode: params[:data][:real_estate_listing][:zipcode]
    )

    if info_request.save
      render status: 200, json: {}
    else
      render status: 400, json: {}
    end
  end

  def app_view_visit
    view_visit = AppViewVisit.new(
      event_type: params[:event_type],
      reference_id: params[:data][:reference_id],
      user_id: params[:data][:user_id],
      app_id: params[:data][:app_id],
      device_id: params[:data][:device_id],
      occurred_at: params[:data][:occurred_at],
      real_estate_listing_id: params[:data][:real_estate_listing][:id],
      real_estate_listing_url: params[:data][:real_estate_listing][:url],
      real_estate_listing_mls_number: params[:data][:real_estate_listing][:mls_number],
      real_estate_listing_mls: params[:data][:real_estate_listing][:mls],
      real_estate_listing_address: params[:data][:real_estate_listing][:address],
      real_estate_listing_city: params[:data][:real_estate_listing][:city],
      real_estate_listing_state: params[:data][:real_estate_listing][:state],
      real_estate_listing_zipcode: params[:data][:real_estate_listing][:zipcode]
    )

    if view_visit.save
      render status: 200, json: {}
    else
      render status: 400, json: {}
    end
  end

  def selected_agent
    selected_agent = SelectedAgent.new(
      event_type: params[:event_type],
      reference_id: params[:data][:reference_id],
      app_id: params[:data][:app_id],
      device_id: params[:data][:device_id],
      user_id: params[:data][:user_id],
      agent_email: params[:data][:agent_email],
      occurred_at: params[:data][:occurred_at]
    )

    if selected_agent.save
      render status: 200, json: {}
    else
      render status: 400, json: {}
    end
  end
end
