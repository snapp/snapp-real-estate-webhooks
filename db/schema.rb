# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160822211709) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "app_sessions", force: :cascade do |t|
    t.string   "event_type"
    t.string   "reference_id"
    t.integer  "app_id"
    t.string   "device_id"
    t.integer  "user_id"
    t.string   "device_type"
    t.string   "device_os"
    t.string   "timezone"
    t.string   "locale"
    t.string   "language"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.datetime "occurred_at"
  end

  create_table "app_view_visits", force: :cascade do |t|
    t.string   "event_type"
    t.string   "reference_id"
    t.integer  "app_id"
    t.string   "device_id"
    t.integer  "user_id"
    t.string   "view_type"
    t.integer  "real_estate_listing_id"
    t.string   "real_estate_listing_url"
    t.string   "real_estate_listing_mls_number"
    t.string   "real_estate_listing_mls"
    t.string   "real_estate_listing_address"
    t.string   "real_estate_listing_city"
    t.string   "real_estate_listing_state"
    t.string   "real_estate_listing_zipcode"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.datetime "occurred_at"
  end

  create_table "real_estate_listing_favorites", force: :cascade do |t|
    t.string   "event_type"
    t.string   "reference_id"
    t.integer  "app_id"
    t.string   "device_id"
    t.integer  "user_id"
    t.integer  "real_estate_listing_id"
    t.string   "real_estate_listing_url"
    t.string   "real_estate_listing_mls_number"
    t.string   "real_estate_listing_mls"
    t.string   "real_estate_listing_address"
    t.string   "real_estate_listing_city"
    t.string   "real_estate_listing_state"
    t.string   "real_estate_listing_zipcode"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.datetime "occurred_at"
  end

  create_table "real_estate_listing_info_requests", force: :cascade do |t|
    t.string   "event_type"
    t.string   "reference_id"
    t.integer  "app_id"
    t.string   "device_id"
    t.integer  "user_id"
    t.string   "agent_email"
    t.string   "user_email"
    t.string   "user_name"
    t.string   "user_phone"
    t.string   "user_question"
    t.string   "requested_showing_date"
    t.integer  "real_estate_listing_id"
    t.string   "real_estate_listing_url"
    t.string   "real_estate_listing_mls_number"
    t.string   "real_estate_listing_mls"
    t.string   "real_estate_listing_address"
    t.string   "real_estate_listing_city"
    t.string   "real_estate_listing_state"
    t.string   "real_estate_listing_zipcode"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.datetime "occurred_at"
  end

  create_table "real_estate_listing_searches", force: :cascade do |t|
    t.string   "event_type"
    t.string   "reference_id"
    t.integer  "app_id"
    t.string   "device_id"
    t.integer  "user_id"
    t.string   "search_type"
    t.json     "search_params"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.datetime "occurred_at"
  end

  create_table "real_estate_listing_unfavorites", force: :cascade do |t|
    t.string   "event_type"
    t.string   "reference_id"
    t.integer  "app_id"
    t.string   "device_id"
    t.integer  "user_id"
    t.integer  "real_estate_listing_id"
    t.string   "real_estate_listing_url"
    t.string   "real_estate_listing_mls_number"
    t.string   "real_estate_listing_mls"
    t.string   "real_estate_listing_address"
    t.string   "real_estate_listing_city"
    t.string   "real_estate_listing_state"
    t.string   "real_estate_listing_zipcode"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.datetime "occurred_at"
  end

  create_table "selected_agents", force: :cascade do |t|
    t.string   "event_type"
    t.string   "reference_id"
    t.integer  "app_id"
    t.string   "device_id"
    t.integer  "user_id"
    t.string   "agent_email"
    t.datetime "occurred_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "user_sign_ins", force: :cascade do |t|
    t.string   "event_type"
    t.string   "reference_id"
    t.integer  "app_id"
    t.integer  "user_id"
    t.string   "device_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.datetime "occurred_at"
    t.string   "selected_agent_email"
  end

  create_table "user_sign_ups", force: :cascade do |t|
    t.string   "event_type"
    t.string   "reference_id"
    t.integer  "app_id"
    t.integer  "user_id"
    t.string   "device_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.datetime "occurred_at"
    t.string   "selected_agent_email"
  end

end
