class AddSelectedAgentEmailToAuthWebhooks < ActiveRecord::Migration[5.0]
  def change
    add_column :user_sign_ups, :selected_agent_email, :string
    add_column :user_sign_ins, :selected_agent_email, :string
  end
end
