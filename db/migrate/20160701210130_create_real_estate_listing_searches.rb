class CreateRealEstateListingSearches < ActiveRecord::Migration[5.0]
  def change
    create_table :real_estate_listing_searches do |t|
      t.string :event_type
      t.string :reference_id
      t.integer :app_id
      t.string :device_id
      t.integer :user_id
      t.string :search_type
      t.json :search_params

      t.timestamps
    end
  end
end
