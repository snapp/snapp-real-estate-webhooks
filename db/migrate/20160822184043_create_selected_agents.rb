class CreateSelectedAgents < ActiveRecord::Migration[5.0]
  def change
    create_table :selected_agents do |t|
      t.string :event_type
      t.string :reference_id
      t.integer :app_id
      t.string :device_id
      t.integer :user_id
      t.string :agent_email
      t.datetime :occurred_at

      t.timestamps
    end
  end
end
