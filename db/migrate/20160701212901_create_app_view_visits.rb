class CreateAppViewVisits < ActiveRecord::Migration[5.0]
  def change
    create_table :app_view_visits do |t|
      t.string :event_type
      t.string :reference_id
      t.integer :app_id
      t.string :device_id
      t.integer :user_id
      t.string :view_type
      t.integer :real_estate_listing_id
      t.string :real_estate_listing_url
      t.string :real_estate_listing_mls_number
      t.string :real_estate_listing_mls
      t.string :real_estate_listing_address
      t.string :real_estate_listing_city
      t.string :real_estate_listing_state
      t.string :real_estate_listing_zipcode


      t.timestamps
    end
  end
end
