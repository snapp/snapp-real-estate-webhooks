class CreateAppSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :app_sessions do |t|
      t.string :event_type
      t.string :reference_id
      t.integer :app_id
      t.string :device_id
      t.integer :user_id
      t.string :device_type
      t.string :device_os
      t.string :timezone
      t.string :locale
      t.string :language

      t.timestamps
    end
  end
end
