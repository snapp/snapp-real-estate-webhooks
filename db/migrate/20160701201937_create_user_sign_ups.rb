class CreateUserSignUps < ActiveRecord::Migration[5.0]
  def change
    create_table :user_sign_ups do |t|
      t.string :event_type
      t.string :reference_id
      t.integer :app_id
      t.integer :user_id
      t.string :device_id
      t.string :first_name
      t.string :last_name
      t.string :email

      t.timestamps
    end
  end
end
