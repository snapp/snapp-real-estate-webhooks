class AddOccurredAtToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :app_sessions, :occurred_at, :datetime
    add_column :app_view_visits, :occurred_at, :datetime
    add_column :real_estate_listing_favorites, :occurred_at, :datetime
    add_column :real_estate_listing_info_requests, :occurred_at, :datetime
    add_column :real_estate_listing_searches, :occurred_at, :datetime
    add_column :real_estate_listing_unfavorites, :occurred_at, :datetime
    add_column :user_sign_ins, :occurred_at, :datetime
    add_column :user_sign_ups, :occurred_at, :datetime
  end
end
